# flutter_checks_gitlab_ci

Example repository to the post [Run checks for Flutter projects with GitLab CI](https://dev.to/arthurdenner/run-checks-for-flutter-projects-with-gitlab-ci-1i42). Results [here](https://gitlab.com/arthurdenner/flutter_checks_gitlab_ci/-/merge_requests/1).

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
